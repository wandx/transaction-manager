<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;
    protected $dates = ["date_paid"];

    public function transaction_items(){
        return $this->hasMany(TransactionItem::class);
    }
}
