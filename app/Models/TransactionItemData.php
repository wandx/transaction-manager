<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class TransactionItemData extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public function transaction_item(){
        return $this->belongsTo(TransactionItem::class,"transaction_item_id");
    }
}
