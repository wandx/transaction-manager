<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    use UuidForKey;

    protected $guarded = ["id"];
    public $incrementing = false;

    public function transaction(){
        return $this->belongsTo(Transaction::class);
    }

    public function transaction_item_data(){
        return $this->hasMany(TransactionItemData::class,"transaction_item_id");
    }

    public function data_string(){
        $data = [];
        if($this->transaction_item_data != null){
            foreach ($this->transaction_item_data as $d){
                $x = [
                    "name" => $d->name,
                    "amount" => $d->amount
                ];
                $data[] = $x;
            }
        }
        return json_encode($data);
    }
}
