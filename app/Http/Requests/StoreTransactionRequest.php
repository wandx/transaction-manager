<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "description" => "required",
            "code" => "required|numeric",
            "rate_euro" => "required|numeric",
            "date_paid" => "required|date_format:d/m/Y",
            "category" => "required|array|min:1",
            "trans_data"=>"required|array|min:1"
        ];
    }
}
