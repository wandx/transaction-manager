<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginCont extends Controller
{
    public function index(){
        return view("admin.login");
    }

    public function doLogin(Request $request){
        $validator = app("validator")->make($request->all(),[
            "username"=>"required",
            "password" => "required"
        ]);

        if($validator->fails()){
            return back()->withErrors($validator->messages())->withInput();
        }

        if(!auth()->attempt($request->only(["username","password"]))){
            return back()->withErrors(["failed"=>"Periksa kembali username/password"]);
        }

        return redirect()->route("admin.transaction.index")->withSuccess("Selamat datang ".auth()->user()->name);
    }

    public function logout(){
        auth()->logout();
        return redirect()->route("admin.transaction.index")->withSuccess("Anda telah logout.");
    }
}
