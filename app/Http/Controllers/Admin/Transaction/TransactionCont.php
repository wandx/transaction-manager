<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Http\Requests\StoreTransactionRequest;
use App\Models\Transaction;
use App\Models\TransactionItemData;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionCont extends Controller
{
    public function index(Request $request,TransactionItemData $itemData){
        $itemData = $itemData->newQuery();
        $opt = [
            "" => "All Category",
            "income" => "INCOME",
            "expense" => "EXPENSE"
        ];
        $per_page = 10;

        if($request->has("per_page") && $request->get("per_page") != ""){
            $per_page = $request->get("per_page");
        }



        $where = function($q) use ($request){
            if($request->has("term") && $request->get("term") != ""){
                foreach (explode(" ",$request->get("term")) as $term){
                    $q->whereHas("transaction_item",function($td) use ($term){
                        $td->whereHas("transaction",function($t) use ($term){
                            $t->where("description","like","%".$term."%");
                        });
                    });

                    $q->orWhere("name","like","%".$term."%");
                }
            }

            if($request->has("start_date") && $request->get("start_date") != ""){
                $q->whereHas("transaction_item",function($ti) use ($request){
                    $ti->whereHas("transaction",function($t) use ($request){
                        $t->whereDate("date_paid",">=",Carbon::createFromFormat("d/m/Y",$request->get("start_date"))->format("Y-m-d"));
                    });
                });
            }

            if($request->has("end_date") && $request->get("end_date") != ""){
                $q->whereHas("transaction_item",function($ti) use ($request){
                    $ti->whereHas("transaction",function($t) use ($request){
                        $t->whereDate("date_paid","<=",Carbon::createFromFormat("d/m/Y",$request->get("end_date"))->format("Y-m-d"));
                    });
                });
            }

            if($request->has("category") && $request->get("category") != ""){
                $q->whereHas("transaction_item",function($ti) use ($request){
                    $ti->where("category",$request->get("category"));
                });
            }
        };

        $itemData->where($where);
        $itemData->with("transaction_item.transaction");
        $itemData = $itemData->paginate($per_page)->appends($request->query());

        return view("admin.transaction.index",compact("itemData","opt"));
    }
    public function add(){
        $opt = [
            "income" => "INCOME",
            "expense" => "EXPENSE"
        ];
        $transaction = null;
        return view("admin.transaction.add",compact("opt","transaction"));
    }

    public function edit($id,Transaction $transaction){
        $opt = [
            "income" => "INCOME",
            "expense" => "EXPENSE"
        ];
        $transaction = $transaction->newQuery()->findOrFail($id);
        return view("admin.transaction.edit",compact("opt","transaction"));
    }

    public function recap(Request $request, Transaction $transaction){
        $transaction = $transaction->newQuery();
        $opt = [
            "" => "All Category",
            "income" => "INCOME",
            "expense" => "EXPENSE"
        ];

        $where = function($q) use ($request){
            if($request->has("term") && $request->get("term") != ""){
                $q->where("description","like","%".$request->get("term")."%");
            }

            if($request->has("start_date") && $request->get("start_date") != ""){
                $q->whereDate("date_paid",">=",Carbon::createFromFormat("d/m/Y",$request->get("start_date"))->format("Y-m-d"));
            }

            if($request->has("end_date") && $request->get("end_date") != ""){
                $q->whereDate("date_paid","<=",Carbon::createFromFormat("d/m/Y",$request->get("end_date"))->format("Y-m-d"));
            }

            if($request->has("category") && $request->get("category") != ""){
                $q->where("category",$request->get("category"));
            }
        };


        $itemData = $transaction
                ->join("transaction_items as ti","ti.transaction_id","=","transactions.id")
                ->join("transaction_item_datas as tid","tid.transaction_item_id","=","ti.id")
                ->groupBy(["ti.category","tid.transaction_item_id"])
                ->selectRaw("transactions.*,SUM(tid.amount) as total,ti.category")
                ->orderBy("transactions.created_at","asc")
                ->where($where)
                ->paginate(10);

//        return $transaction;

        return view("admin.transaction.recap",compact("itemData","opt"));
    }

    public function store(StoreTransactionRequest $request,Transaction $transaction){
        if($request->has("date_paid")){
            $request->merge(["date_paid"=>Carbon::createFromFormat("d/m/Y",$request->input("date_paid"))->format("Y-m-d")]);
        }
        $data = $request->except(["id","_token","category","trans_data"]);
        DB::beginTransaction();

        try{

            $store = $transaction->newQuery()->updateOrCreate(["id"=>$request->input("id")],$data);
//            return $store;
            if($request->has("id")){
                $clear = $transaction->newQuery()->find($store->id);
                $clear->transaction_items()->delete();
            }


            foreach ($request->input("category") as $k=>$category) {
                $store_category = $store->transaction_items()->create([
                    "category" => $category
                ]);

                $transaction_item_data = $request->input("trans_data");
                $transaction_item_data = json_decode($transaction_item_data[$k],true);
                foreach ($transaction_item_data as $v){
                    $store_category->transaction_item_data()->create($v);
                }
            }


            DB::commit();
        }catch (Exception $e){
            DB::rollBack();
            return back()->withErrors(["failed"=>"Periksa kembali data transaksi."])->withInput();
        }

        return redirect()->route("admin.transaction.index")->withSuccess("Data tersimpan.");
    }

    public function destroy_transaction_data($id,TransactionItemData $itemData){
        $itemData->newQuery()->findOrFail($id)->delete();
        return back()->withSuccess("Data telah dihapus");
    }
}
