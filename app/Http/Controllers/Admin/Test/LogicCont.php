<?php

namespace App\Http\Controllers\Admin\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogicCont extends Controller
{
    public function index(){
        $amount  = 1895250;

        return [
            "amount" => $amount,
            "data" => $this->getFragment($amount)
        ];
    }

    public function junior(){
        return view("admin.test.junior");
    }

    private function getFragment($amount){
        $available = [
            100000,50000,20000,5000,100,50
        ];

        $data = [];
        foreach ($available as $item){
            $mod = $amount%$item;
            $amountFactor = ($amount-$mod)/$item;
            $save = [
                "pecahan" => $item,
                "jumlah" => $amountFactor
            ];
            $data[] = $save;
            $amount = $mod;
        }
        return $data;
    }
}
