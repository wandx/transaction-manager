<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionItemDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_item_datas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid("transaction_item_id");
            $table->string("name");
            $table->string("amount");
            $table->timestamps();

            $table->foreign("transaction_item_id")->references("id")->on("transaction_items")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_item_datas');
    }
}
