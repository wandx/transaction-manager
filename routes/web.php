<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("admin.transaction.index");
});

Route::group(["namespace"=>"Admin","as"=>"admin."],function(){
    Route::get("login","LoginCont@index")->name("login");
    Route::post("login","LoginCont@doLogin")->name("login.post");
    Route::get("logout","LoginCont@logout")->name("logout");

    Route::group(["prefix"=>"transaction","namespace"=>"Transaction","as"=>"transaction."],function(){
        Route::get("/","TransactionCont@index")->name("index");

        Route::group(["middleware"=>"auth"],function(){
            Route::get("recap","TransactionCont@recap")->name("recap");
            Route::get("add","TransactionCont@add")->name("add");
            Route::post("add","TransactionCont@store")->name("store");
            Route::get("{id}/edit","TransactionCont@edit")->name("edit");
            Route::post("{id}/edit","TransactionCont@store")->name("update");
            Route::get("{id}/delete","TransactionCont@destroy_transaction_data")->name("delete_transaction_data");
        });

    });

    Route::group(["prefix"=>"test","namespace"=>"Test","as"=>"test."],function(){
        Route::get("logic","LogicCont@index")->name("test-logic");
        Route::get("junior","LogicCont@junior")->name("test-junior");
    });
});