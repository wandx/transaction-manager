const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .styles([
        "resources/css/bootstrap.min.css",
        "resources/css/font-awesome.min.css",
        "resources/css/ionicons.min.css",
        "resources/css/bootstrap-datetimepicker.min.css",
        "resources/css/AdminLTE.min.css",
        "resources/css/skins/skin-blue.css",
        "resources/css/toastr.min.css",
        "resources/css/wandx.css",
    ],"public/css/site.css")

    .scripts([
        "resources/js/jquery.min.js",
        "resources/js/bootstrap.min.js",
        "resources/js/jquery.slimscroll.min.js",
        "resources/js/fastclick.js",
        "resources/js/adminlte.min.js",
        "resources/js/moment-with-locales.min.js",
        "resources/js/bootstrap-datetimepicker.min.js",
        "resources/js/toastr.min.js",
        "resources/js/functions.js",
        "resources/js/wandx.js"
    ],"public/js/site.js")

    .disableNotifications();
