<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-body">
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label("description","Description",["class"=>"control-label col-sm-2"]) !!}
                        <div class="col-sm-10">
                            {!! Form::textarea("description",null,["class"=>"form-control","required","rows"=>2]) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label("code","Code",["class"=>"control-label col-sm-3"]) !!}
                        <div class="col-sm-9">
                            {!! Form::text("code",null,["class"=>"form-control","required"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("rate_euro","Rate Euro",["class"=>"control-label col-sm-3"]) !!}
                        <div class="col-sm-9">
                            {!! Form::text("rate_euro",null,["class"=>"form-control","required"]) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label("date_paid","Date Paid",["class"=>"control-label col-sm-3"]) !!}
                        <div class="col-sm-9">
                            <div class="input-group" id="date-paid">
                                {!! Form::text("date_paid",$transaction != null ? $transaction->date_paid->format("d/m/Y"):null,["class"=>"form-control","required"]) !!}
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Data Transaksi</h3>
            </div>
            <div class="panel-body">
                <div class="transaction-item-container">
                    @if($transaction != null)
                        @foreach($transaction->transaction_items as $item)
                            <div class="panel panel-danger">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12 text-right">
                                            <button type="button" class="btn btn-xs btn-danger text-bold" onclick="removeTransItemRow(event)">x</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-3">Category</label>
                                            <div class="col-sm-3">
                                                {!! Form::select("category[]",$opt,$item->category,["class"=>"form-control","required"]) !!}
                                                {!! Form::hidden("trans_data[]",$item->data_string(),["class"=>"trans_data"]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="control-label col-sm-3">&nbsp;</label>
                                            <div class="col-sm-7">
                                                <div class="rTable">
                                                    <div class="rTableRow">
                                                        <div class="rTableHead"><strong>Nama Transaksi</strong></div>
                                                        <div class="rTableHead text-right"><strong>Nominal</strong></div>
                                                    </div>
                                                    <div class="row-container">
                                                        @foreach($item->transaction_item_data as $itemData)
                                                            <div class="rTableRow">
                                                                <div class="rTableCell">{{ $itemData->name }}</div>
                                                                <div class="rTableCell text-right"><span>{{ $itemData->amount }} </span> &nbsp;&nbsp;<i onclick="removeTransDataRow(event)" class="fa fa-trash cursor-pointer"></i></div>
                                                            </div>
                                                        @endforeach
                                                    </div>

                                                    <div class="rTableRow">
                                                        <div class="rTableCell"><input type="text" class="form-control transaction-name" {{ $transaction == null ? "required":"" }}></div>
                                                        <div class="rTableCell">
                                                            <div class="input-group">
                                                                <input type="number" class="form-control transaction-amount" {{ $transaction == null ? "required":"" }}>
                                                                <div class="input-group-btn">
                                                                    <button type="button" onclick="addTransData(event)" class="btn btn-info text-bold">+</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="panel panel-danger">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-12 text-right">
                                        <button type="button" class="btn btn-xs btn-danger text-bold" onclick="removeTransItemRow(event)">x</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-3">Category</label>
                                        <div class="col-sm-3">
                                            {!! Form::select("category[]",$opt,null,["class"=>"form-control","required"]) !!}
                                            {!! Form::hidden("trans_data[]",null,["class"=>"trans_data"]) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label col-sm-3">&nbsp;</label>
                                        <div class="col-sm-7">
                                            <div class="rTable">
                                                <div class="rTableRow">
                                                    <div class="rTableHead"><strong>Nama Transaksi</strong></div>
                                                    <div class="rTableHead text-right"><strong>Nominal</strong></div>
                                                </div>
                                                <div class="row-container">

                                                </div>

                                                <div class="rTableRow">
                                                    <div class="rTableCell"><input type="text" class="form-control transaction-name" required></div>
                                                    <div class="rTableCell">
                                                        <div class="input-group">
                                                            <input type="number" class="form-control transaction-amount" required>
                                                            <div class="input-group-btn">
                                                                <button type="button" onclick="addTransData(event)" class="btn btn-info text-bold">+</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>

                <div class="row">
                    <div class="col-sm-12 text-right">
                        <button type="button" onclick="addTransItem(event)" class="btn btn-primary">Tambah</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="{{ route("admin.transaction.index") }}" onclick="return confirm('Batal ?')" class="btn btn-danger">Batal</a>
    </div>
</div>

@push("scripts")
    <script>
        $(function(){
            $("#date-paid").datetimepicker({
                format:"DD/MM/YYYY",
                locale:'id'
            });
        });
    </script>
@endpush