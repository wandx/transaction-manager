<template id="master-transaction-data">
    <div class="rTableRow">
        <div class="rTableCell">%s</div>
        <div class="rTableCell text-right"><span>%s </span> &nbsp;&nbsp;<i onclick="removeTransDataRow(event)" class="fa fa-trash cursor-pointer"></i></div>
    </div>
</template>

<template id="master-transaction-item">
    <div class="panel panel-danger">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 text-right">
                    <button class="btn btn-xs btn-danger text-bold" onclick="removeTransItemRow(event)">x</button>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="" class="control-label col-sm-3">Category</label>
                    <div class="col-sm-3">
                        {!! Form::select("category[]",$opt,null,["class"=>"form-control","required"]) !!}
                        {!! Form::hidden("trans_data[]",null,["class"=>"trans_data"]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="control-label col-sm-3">&nbsp;</label>
                    <div class="col-sm-7">
                        <div class="rTable">
                            <div class="rTableRow">
                                <div class="rTableHead"><strong>Nama Transaksi</strong></div>
                                <div class="rTableHead text-right"><strong>Nominal</strong></div>
                            </div>
                            <div class="row-container">

                            </div>

                            <div class="rTableRow">
                                <div class="rTableCell"><input type="text" class="form-control transaction-name" {{ $transaction == null ? "required":"" }}></div>
                                <div class="rTableCell">
                                    <div class="input-group">
                                        <input type="number" class="form-control transaction-amount" {{ $transaction == null ? "required":"" }}>
                                        <div class="input-group-btn">
                                            <button type="button" onclick="addTransData(event)" class="btn btn-info text-bold">+</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>