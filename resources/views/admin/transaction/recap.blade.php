@extends("layouts.admin")
@section("page_header","Rekap Transaksi")
@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data Transaksi</a></li>
        <li class="active">Rekap Transaksi</li>
    </ol>
@stop

@push("contents")
    <div class="row" id="filter">
        <div class="col-sm-2">
            {{--<a href="{{ route("admin.transaction.add") }}" class="btn btn-primary">Tambah Transaksi</a>--}}
        </div>
        <div class="col-sm-10">
            <form class="row" id="form-filter">
                <input type="hidden" name="per_page" id="per_page" value="{{ Request::get("per_page") }}">
                <div class="col-sm-3">
                    <div class="input-group" id="sd">
                        <input type="text" class="form-control" id="start-date" name="start_date" value="{{ Request::get("start_date") }}" placeholder="Start Date">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="input-group" id="ed">
                        <input type="text" class="form-control" id="end-date" name="end_date" value="{{ Request::get("end_date") }}" placeholder="End Date">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>

                <div class="col-sm-2">
                    {!! Form::select("category",$opt,Request::get("category"),['class'=>'form-control']) !!}
                </div>

                <div class="col-sm-3">
                    <input type="text" class="form-control" id="term" name="term" value="{{ Request::get("term") }}" placeholder="Keywords">
                </div>

                <div class="col-sm-1">
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="bg-gray">
                            <th>No</th>
                            <th>Deskripsi</th>
                            <th>Tanggal</th>
                            <th>Kategori</th>
                            <th class="text-right">Nominal(IDR)</th>
                        </tr>
                        </thead>
                        @forelse($itemData as $item)
                            <tr>
                                <td>{{ ($itemData->currentPage()-1) * $itemData->perPage() + $loop->index + 1 }}</td>
                                <td>{{ $item->description }}</td>
                                <td>{{ $item->date_paid->format("d/m/Y") }}</td>
                                <td>{{ $item->category }}</td>
                                <td class="text-right">{{ nf($item->total)}}</td>
                            </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="5">Tidak ada data.</td>
                                </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
@endpush

@push("footer")
    <div class="row">
        <div class="col-sm-6">
            {!! Form::select("per_page",[10=>10,20=>20,50=>50],Request::get("per_page"),["onchange"=>"populatePerPage(event)"]) !!}
            &nbsp;
            Menampilkan {{ $itemData->count() }} dari {{ $itemData->total() }} data
        </div>
        <div class="col-sm-6 text-right">
            {!! $itemData->render() !!}
        </div>
    </div>
@endpush

@push("scripts")
    <script type="text/javascript">
        $(function () {
            var sd = $('#sd').datetimepicker({
                format:"DD/MM/YYYY"
            });
            var ed = $('#ed').datetimepicker({
                format:"DD/MM/YYYY",
                useCurrent: false //Important! See issue #1075
            });
            sd.on("dp.change", function (e) {
                ed.data("DateTimePicker").minDate(e.date);
            });
            ed.on("dp.change", function (e) {
                sd.data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endpush