@extends("layouts.admin")
@section("page_header","Input Data Transaksi")
@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data Transaksi</a></li>
        <li class="active">Input Data Transaksi</li>
    </ol>
@stop
@push("contents")
    {!! Form::open(["class"=>"form-horizontal","onsubmit"=>"submitTransaction(event)"]) !!}
    @include("admin.transaction.form")
    {!! Form::close() !!}
    @include("admin.transaction.template")

@endpush