@extends("layouts.admin")
@section("page_header","Edit Data Transaksi")
@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data Transaksi</a></li>
        <li><a href="{{ route("admin.transaction.index") }}"><i class="fa fa-dashboard"></i> List Data Transaksi</a></li>
        <li class="active">Edit Data Transaksi</li>
    </ol>
@stop
@push("contents")
    {!! Form::model($transaction,["class"=>"form-horizontal","onsubmit"=>"submitTransaction(event)"]) !!}
    {!! Form::hidden("id",$transaction->id) !!}
    @include("admin.transaction.form")
    {!! Form::close() !!}
    @include("admin.transaction.template")

@endpush