<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Test Junior</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset("css/junior.css") }}">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Homepage</a></li>
                <li><a href="#">News</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        Produk
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Google</a></li>
                        <li><a href="#">Facebook Ads</a></li>
                        <li><a href="#">SEO</a></li>
                        <li><a href="#">Training</a></li>
                    </ul>
                </li>
                <li><a href="#">Pemesanan</a></li>
                <li><a href="#">Kontak</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>

<div class="row">
    <img src="http://placehold.it/1600x500" alt="" class="img-responsive">
</div>
<br>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-3">
            <div class="my-box">Google AdWords</div>
        </div>
        <div class="col-sm-3">
            <div class="my-box">Facebook Ads</div>
        </div>
        <div class="col-sm-3">
            <div class="my-box">SEO</div>
        </div>
        <div class="col-sm-3">
            <div class="my-box">Training</div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <!-- Left-aligned -->
            <div class="media">
                <div class="media-left">
                    <img src="http://placehold.it/300x200" class="media-object">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Google AdWords</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        <a href="#"><b>Read More</b></a>
                    </p>
                </div>
            </div>

            <!-- Right-aligned -->
            <div class="media">
                <div class="media-body">
                    <h4 class="media-heading">Google AdWords</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        <a href="#"><b>Read More</b></a>
                    </p>
                </div>
                <div class="media-right">
                    <img src="http://placehold.it/300x200" class="media-object">
                </div>
            </div>

            <!-- Left-aligned -->
            <div class="media">
                <div class="media-left">
                    <img src="http://placehold.it/300x200" class="media-object">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Google AdWords</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        <a href="#"><b>Read More</b></a>
                    </p>
                </div>
            </div>

            <!-- Right-aligned -->
            <div class="media">
                <div class="media-body">
                    <h4 class="media-heading">Google AdWords</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium consequatur dicta distinctio ducimus ea eius illo iusto minima nemo numquam odio odit, placeat ratione soluta temporibus tenetur veritatis, vitae voluptatibus.
                        <a href="#"><b>Read More</b></a>
                    </p>
                </div>
                <div class="media-right">
                    <img src="http://placehold.it/300x200" class="media-object">
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>