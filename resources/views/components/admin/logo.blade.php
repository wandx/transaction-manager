<!-- Logo -->
<a href="{{ $url ?? "#" }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">
        {{ $logoMini ?? "ALTE" }}
    </span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">{{ $logoLg ?? "AdminLTE" }}</span>
</a>