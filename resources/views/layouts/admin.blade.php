<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield("title","Admin")</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset("css/site.css") }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="/" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">TJOO</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">TONJOO</span>
            </a>

            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        @auth
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ asset("img/avatar.png") }}" class="user-image" alt="User Image">
                                <span class="hidden-xs">{{ auth()->user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{ asset("img/avatar.png") }}" class="img-circle" alt="User Image">

                                    <p>
                                        {{ auth()->user()->name }}
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">

                                    <a href="{{ route("admin.logout") }}" class="btn btn-default btn-flat  btn-block">Sign out</a>
                                </li>
                            </ul>
                        </li>
                        @endauth

                        @guest
                            <li class="">
                                <a href="{{ route("admin.login") }}">Login</a>
                            </li>
                        @endguest
                        <!-- Control Sidebar Toggle Button -->
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    <li>
                        <a href="#">
                            <i class="fa fa-th"></i> <span>Dashboard</span>
                        </a>
                    </li>

                    @auth
                        <li class="treeview {{ Request::segment(1) == "transaction" ? "active":"" }}">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Data Transaksi</span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="{{ Request::routeIs("admin.transaction.add") ? "active":"" }}"><a href="{{ route("admin.transaction.add") }}"><i class="fa fa-circle-o"></i> Tambah Data Transaksi</a></li>
                                <li class="{{ Request::routeIs("admin.transaction.index") ? "active":"" }}"><a href="{{ route("admin.transaction.index") }}"><i class="fa fa-circle-o"></i> List Data Transaksi</a></li>
                                <li class="{{ Request::routeIs("admin.transaction.recap") ? "active":"" }}"><a href="{{ route("admin.transaction.recap") }}"><i class="fa fa-circle-o"></i> Rekap Data Transaksi</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route("admin.test.test-logic") }}" target="_blank"><i class="fa fa-puzzle-piece"></i> Test Logika</a></li>
                        <li><a href="{{ route("admin.test.test-junior") }}" target="_blank"><i class="fa fa-ambulance"></i> Test Junior</a></li>
                    @endauth

                    @guest
                        <li class="{{ Request::segment(1) == "transaction" ? "active":"" }}">
                            <a href="{{ route("admin.transaction.index") }}">
                                <i class="fa fa-dashboard"></i> <span>Data Transaksi</span>
                            </a>
                        </li>
                    @endguest
                </ul>
            </section>
        </aside>

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield("page_header","Page Header")
                    <small>@yield("page_sub_header")</small>
                </h1>
                @yield("breadcrumb")
            </section>

            <!-- Main content -->
            <section class="content">
                @stack("contents")
            </section>
        </div>

        <footer class="main-footer">
            @stack("footer")
            {{--<div class="pull-right hidden-xs">--}}
                {{--<b>Version</b> 2.4.0--}}
            {{--</div>--}}
            {{--<strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights--}}
            {{--reserved.--}}
        </footer>

    </div>


    <script src="{{ asset("js/site.js") }}"></script>
    <script>
        $(document).ready(function () {
            $('.sidebar-menu').tree()
        })
    </script>
    @stack("scripts")
    @include("admin.partial.errors")
    @include("admin.partial.success")
</body>
</html>