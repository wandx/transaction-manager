// interpolate string
function interpolate(theString, argumentArray) {
    var regex = /%s/;
    var _r = function(p,c){
        return p.replace(regex,c);
    };

    return argumentArray.reduce(_r, theString);
}

function buildStringData(elem){
    var data = [];
    var rows = elem.find(".rTableRow");

    if(rows.length > 0){
        rows.each(function(i,e){
            var listElement = $(e).find(".rTableCell");
            console.log("name",$(listElement[0]).html());
            console.log("amount",);
            var d = {
                name: $(listElement[0]).html(),
                amount: $(listElement[1]).find("span").html()
            };

            data.push(d);
        });
    }

    return JSON.stringify(data);
}