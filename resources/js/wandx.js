function addTransData(e){
    var masterTransData = $("#master-transaction-data"),
        btn = $(e.target),
        name = btn.parent().parent().parent().parent().find(".transaction-name"),
        amount = btn.parent().parent().parent().parent().find(".transaction-amount"),
        rowContainer = btn.parent().parent().parent().parent().parent().find(".row-container"),
        mtdString = masterTransData.html(),
        dataLength;

    // validasi
    if(name.val() === ""){
        alert("Nama transaksi diperlukan");
        return false;
    }

    if(amount.val() === ""){
        alert("Nominal diperlukan");
        return false;
    }

    // clean
    rowContainer.append(interpolate(mtdString,[name.val(),amount.val()]));
    var stringData = buildStringData(rowContainer); // build string data for data transaction
    rowContainer.parent().parent().parent().parent().find(".trans_data").val(stringData); //assign string data to input

    // require/not require input
    dataLength  = rowContainer.find(".rTableRow").length;
    if(dataLength == 0){
        name.prop("required",true);
        amount.prop("required",true);
    }else{
        name.prop("required",false);
        amount.prop("required",false);
    }

    name.val("").trigger("focus");
    amount.val("");
}

function removeTransDataRow(e){
    var btn = $(e.target),
        rowContainer = btn.parent().parent().parent(),
        dataLength,
        name = rowContainer.next().find(".transaction-name"),
        amount = rowContainer.next().find(".transaction-amount");

    dataLength  = rowContainer.find(".rTableRow").length - 1;
    btn.parent().parent().remove();

    var stringData = buildStringData(rowContainer); // build string data for data transaction
    rowContainer.parent().parent().parent().parent().find(".trans_data").val(stringData); //assign string data to input

    if(dataLength == 0){
        name.prop("required",true);
        amount.prop("required",true);
    }else{
        name.prop("required",false);
        amount.prop("required",false);
    }

    name.trigger("focus");
}

function addTransItem(e){
    var btn = $(e.target),
        masterTransItem = $("#master-transaction-item"),
        mtiString = masterTransItem.html();

    btn.parent().parent().prev(".transaction-item-container").append(mtiString);
}

function removeTransItemRow(e){
    var btn = $(e.target);

    btn.parent().parent().parent().parent().remove();
}

function populatePerPage(e){
    var sel = $(e.target);
    $("#per_page").val(sel.val());
    $("#form-filter").submit();
}